# ROS2 meeting ideas


## Ideas
* 

## Points
1. ROS presentation [< 10']
   * What is ROS;
   * ROS architecture overview;
   * Why ROS?
   * What is ROS2 and MicroROS:
      * ROS2: quick overview and why is so interesting;
      * MicroROS: quick overview and where is used.

1. Where is used ROS2 [~30']
   * Tipical usege scenario
      * Every sector that requires two or more agents need to communicate to make better informed decisions.
      * Industrial: case study - ROS industrial
      * Automotive: case study - Apex.IO
   * ROS2 in automotive scenario
      * An overview from wp "_A Self-Driving Car Architecture in ROS2_"
      * Example: AMZ Racing Driverless

1. ROS2 use case [~10']
   * MicroRos serial porting in Aurix TC37x
   * Extra demo:
      * Moveit video
      * Autoware video
      * Robot localization
